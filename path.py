import random

from direction import Direction


def is_legal_coord(coord, world_size):
    x, y = coord
    max_x, max_y = world_size

    if x < 0 or x > max_x:
        return False

    if y < 0 or y > max_y:
        return False

    return True


def weighted_choice(choices):
   total = sum(w for c, w in choices)
   r = random.uniform(0, total)
   upto = 0
   for c, w in choices:
      if upto + w >= r:
         return c
      upto += w


class Path(list):
    @staticmethod
    def random_path(start_coord, length, world_size, turn_prob=0.005, diag=False):
        x, y = start_coord

        path = Path()
        path.append((x, y))

        while len(path) < length:
            possible_choices = [coord for coord
                                in [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
                                if coord not in path
                                and is_legal_coord(coord, world_size)]

            weighted_choices = []
            for coord in possible_choices:
                if len(path) < 2:
                    weighted_choices.append((coord, 0.5))
                    continue
                dir1 = Direction.get_dir_from_coords(*path[-2:])
                dir2 = Direction.get_dir_from_coords(coord, path[-1])
                weighted = (coord, turn_prob
                            if Direction.are_turns(dir1, dir2)
                            else 1-turn_prob)
                weighted_choices.append(weighted)

            choice = weighted_choice(weighted_choices)

            x, y = choice
            path.append(choice)

        return path
