from collections import deque
import curses
import random
import sys

from direction import Direction
from path import Path


class Snake:
    """Class represents a snake"""
    direction = Direction.NONE
    position = (0, 0)
    length = 15
    tail = deque()
    isdead = False

    def __init__(self, position):
        self.position = position

    def _change_direction(self, new_direction):
        if not self.direction.is_opposite_direction(new_direction):
            self.direction = new_direction

    def update(self):
        if self.isdead:
            return

        if self.direction == Direction.NONE:
            return

        new_pos = {
            Direction.LEFT:  (self.position[0]-1, self.position[1]),
            Direction.DOWN:  (self.position[0],   self.position[1]+1),
            Direction.RIGHT: (self.position[0]+1, self.position[1]),
            Direction.UP:    (self.position[0],   self.position[1]-1)
        }[self.direction]


        self.tail.appendleft(self.position)

        self.position = new_pos

        if len(self.tail) > self.length:
            self.tail.pop()

    def get_position(self):
        return self.position

    def set_position(self, position):
        self.position = position

    def get_tail(self):
        return self.tail

    def eat(self):
        self.length += 1

    def grow(self, length):
        self.length += length

    def die(self):
        self.isdead = True
        self.end()


class SnakeGame:
    game_over = False
    food = []
    walls = []

    def __init__(self, game_size, difficulty,
                 snake_start=None, walls=None, wrap=True):
        self.game_size = game_size
        self.difficulty = difficulty
        self.wrap = wrap
        self.walls = walls if walls is not None else []

        if snake_start is None:
            snake_start = tuple(map(random.randrange, game_size))

        self.snake = Snake(snake_start)

        self.spawn_food()

    def snake_change_direction(self, direction):
        self.snake._change_direction(direction)

    def snake_grow(self, length=1):
        self.snake.grow(length)

    def snake_eat(self, food):
        self.food.remove(food)
        self.snake_grow()

        self.spawn_food()

        # spawn random extra fruit
        if random.random() < 0.10:
            self.spawn_food()

    def spawn_food(self, x=None, y=None):
        """TODO: Make sure it doesn't spawn in a wall or in the snake"""
        if x is None:
            x = random.randrange(self.game_size[0])
        if y is None:
            y = random.randrange(self.game_size[1])

        self.food.append((x, y))

    def update(self):
        """Game logic"""

        if self.game_over:
            return

        snake_pos = self.snake.get_position()

        if snake_pos in self.snake.tail:
            self.endgame()
            return

        if snake_pos in self.walls:
            self.endgame()
            return

        if snake_pos in self.food:
            self.snake_eat(snake_pos)

        if self.wrap:
            if snake_pos[0] == 0:
                # update tail first
                self.snake.set_position((self.game_size[0],
                                         self.snake.get_position()[1]))

            if snake_pos[0] == self.game_size[0]-1:
                # update tail first
                self.snake.set_position((1, self.snake.get_position()[1]))

            if snake_pos[1] == 0:
                # update tail first
                self.snake.set_position((self.snake.get_position()[0],
                                         self.game_size[1]-1))

            if snake_pos[1] == self.game_size[1]:
                # update tail first
                self.snake.set_position((self.snake.get_position()[0], 1))

        self.snake.update()

    def endgame(self):
        """
        End the game (will be called automatically by the game logic)
        It is the caller's responsibility to check if the game is over
        """
        self.game_over = True


def main_loop(stdscr):
    difficulty = 300
    world_size = tuple(reversed(stdscr.getmaxyx()))
    game = SnakeGame(difficulty=difficulty,
                     game_size=world_size,
                     # walls=Path.random_path(map(random.randrange, world_size),
                                            # 50, world_size),
                     wrap=True)

    while True:
        stdscr.clear()

        game.update()

        if game.game_over:
            break

        for x, y in game.snake.get_tail():
            stdscr.addch(y, x, '+')

        for x, y in game.food:
            stdscr.addch(y, x, '$')

        for x, y in game.walls:
            stdscr.addch(y, x, '=')

        stdscr.addch(*reversed(game.snake.get_position()), '@')

        turn = user_input(stdscr)
        if turn:
            game.snake_change_direction(turn)

        stdscr.timeout(difficulty)


def user_input(stdscr):
    char = stdscr.getch()
    if char == curses.KEY_LEFT:
        return Direction.LEFT

    elif char == curses.KEY_DOWN:
        return Direction.DOWN

    elif char == curses.KEY_RIGHT:
        return Direction.RIGHT

    elif char == curses.KEY_UP:
        return Direction.UP

    elif char == ord('r'):
        return Direction.get_random_direction()

    elif char == ord('q') or char == curses.KEY_RESIZE:
        sys.exit(0)

    return None


def main(stdscr):
    stdscr.nodelay(1)
    curses.curs_set(0)
    main_loop(stdscr)


if __name__ == '__main__':
    curses.wrapper(main)
