import operator

from enum import Enum


class Direction(Enum):
    NONE = 0
    LEFT = 1
    DOWN = 2
    RIGHT = 3
    UP = 4

    def is_turn(self, direction):
        return self.are_turns(self, direction)

    def is_opposite_direction(self, direction):
        return self.are_opposite_directions(self, direction)

    @staticmethod
    def are_turns(direction1, direction2):
        return {
            Direction.LEFT: {
                Direction.LEFT: False,
                Direction.RIGHT: False
            },
            Direction.DOWN: {
                Direction.DOWN: False,
                Direction.UP: False,
            },
            Direction.RIGHT: {
                Direction.RIGHT: False,
                Direction.LEFT: False
            },
            Direction.UP: {
                Direction.UP: False,
                Direction.DOWN: False
            }
        }.get(direction1, {}).get(direction2, True)

    @staticmethod
    def are_opposite_directions(direction1, direction2):
        return {
            Direction.LEFT: {
                Direction.RIGHT: True
            },

            Direction.DOWN: {
                Direction.UP: True
            },

            Direction.RIGHT: {
                Direction.LEFT: True
            },

            Direction.UP: {
                Direction.DOWN: True
            }
        }.get(direction1, {}).get(direction2, False)

    @staticmethod
    def get_random_direction():
        dir_prob = random.randrange(0, 4)
        new_dir = {
            0: Direction.LEFT,
            1: Direction.DOWN,
            2: Direction.RIGHT,
            3: Direction.UP,
        }[dir_prob]
        return new_dir

    @staticmethod
    def get_coord_from_dir(coord, direction):
        dir_to_coord = {
            Direction.NONE:  ( 0,  0),
            Direction.DOWN:  ( 0,  1),
            Direction.UP:    ( 0, -1),
            Direction.RIGHT: ( 1,  0),
            Direction.LEFT:  (-1,  0)
        }

        return tuple(map(operator.add, coord, dir_to_coord.get(direction)))

    @staticmethod
    def get_dir_from_coords(coord1, coord2):
        coord_to_dir = {
            ( 0,  1): Direction.DOWN,
            ( 0, -1): Direction.UP,
            ( 1,  0): Direction.RIGHT,
            (-1,  0): Direction.LEFT
        }

        delta = tuple(map(operator.sub, coord2, coord1))

        return coord_to_dir.get(delta)
